<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cast.index',['data' => DB::table('cast')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required',
            'umur' => 'required'
        ]);

        $validated['bio'] = $request->has('bio') ? $request->bio : NULL;
        $validated['created_at'] = date('Y-m-d H:i:s');

        DB::table('cast')->insert($validated);

        return redirect('/cast');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($cast_id)
    {
        $data = DB::table('cast')->find($cast_id);

        return view('cast.show', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cast_id)
    {
        $edit = DB::table('cast')->find($cast_id);

        return view('cast.edit', ['edit' => $edit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cast_id)
    {
        $validated = $request->validate([
            'nama' => 'required',
            'umur' => 'required'
        ]);

        $validated['bio'] = $request->has('bio') ? $request->bio : NULL;
        $validated['updated_at'] = date('Y-m-d H:i:s');

        DB::table('cast')->where('id', $cast_id)->update($validated);

        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cast_id)
    {
        DB::table('cast')->delete($cast_id);

        return redirect('/cast');
    }
}
