<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul', 100);
            $table->text('ringkasan')->nullable();
            $table->integer('tahun');
            $table->string('poster', 50)->nullable();
            $table->unsignedBigInteger('genre_id')->nullable();
            $table->timestamps();

            $table->foreign('genre_id')->references('id')->on('genre')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film');
    }
}
