@extends('layout.template')

@push('head')
    <style>
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        input[type=number] {
            -moz-appearance: textfield;
        }
        textarea {
            resize: none;
        }
    </style>
@endpush

@section('content')
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Lihat Data Cast</h3>
        </div>
		<div class="card-body">
			<form class="form-horizontal">
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"> Nama : </label>
                    <div class="col-sm-4">
                        <input type="text" name="nama" maxlength="50" class="form-control" placeholder="Nama" readonly required value="{{ $data->nama }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"> Umur : </label>
                    <div class="col-sm-4">
                        <input type="number" name="umur" class="form-control" placeholder="Umur" readonly required value="{{ $data->umur }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"> Bio : </label>
                    <div class="col-sm-4">
                        <textarea name="bio" rows="3" class="form-control" placeholder="Bio" readonly>{{ $data->bio }}</textarea>
                    </div>
                </div>
            </form>
		</div>
        <div class="card-footer">
            <div class="text-right">
                <a href="{{ url('/cast') }}" class="btn btn-secondary">Kembali</a>
            </div>
        </div>
	</div>
@endsection