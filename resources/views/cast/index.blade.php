@extends('layout.template')

@section('content')
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Data Cast</h3>
		</div>
		<div class="card-body">
			<div class="row mb-2" style="margin-right:0 ; margin-left: 0;">
				<a href="{{ url('/cast/create') }}" class="btn btn-primary">
					Tambah Data Cast
				</a>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th style="width: 3%">#</th>
							<th style="width: 30%">Nama</th>
							<th style="width: 9%">Umur</th>
							<th style="width: 35%">Bio</th>
							<th style="width: 13%" class="text-center"><i class="fas fa-cog"></i></th>
						</tr>
					</thead>
					<tbody>
						@foreach ($data as $item)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $item->nama }}</td>
								<td>{{ $item->umur }} tahun</td>
								<td>{{ $item->bio }}</td>
								<td class="text-center">
									<a href="{{ url('cast/' . $item->id) }}" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Detail Cast"data-toggle="tooltip" data-placement="top" title="Detail Cast">
										<i class="far fa-eye"></i>
									</a>
									<a href="{{ url('cast/' . $item->id . '/edit') }}" class="btn btn-warning btn-sm mx-1" data-toggle="tooltip" data-placement="top" title="Edit Cast">
										<i class="fas fa-pen"></i>
									</a>
									<form action="{{ url('cast/' . $item->id) }}" method="POST" style="display: inline">
										@csrf
										@method('DELETE')
										<input type="hidden" name="id" value="{{ $item->id }}">
										<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin menghapus data cast ini?')" data-toggle="tooltip" data-placement="top" title="Hapus Cast">
											<i class="far fa-trash-alt"></i>
										</button>
									</form>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@push('script')
	<script>
		$('[data-toggle="tooltip"]').tooltip();
	</script>
@endpush