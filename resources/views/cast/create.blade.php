@extends('layout.template')

@push('head')
    <style>
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        input[type=number] {
            -moz-appearance: textfield;
        }
        textarea {
            resize: none;
        }
    </style>
@endpush

@section('content')
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Form Tambah Data Cast</h3>
        </div>
		<div class="card-body">
			<form action="{{ url('/cast') }}" method="POST" class="form-horizontal" id="form-create">
                @csrf
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"> Nama : </label>
                    <div class="col-sm-4">
                        <input type="text" name="nama" maxlength="50" class="form-control" placeholder="Nama" required autocomplete="off">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"> Umur : </label>
                    <div class="col-sm-4">
                        <input type="number" name="umur" class="form-control" placeholder="Umur" required autocomplete="off">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"> Bio : </label>
                    <div class="col-sm-4">
                        <textarea name="bio" rows="3" class="form-control" placeholder="Bio"></textarea>
                    </div>
                </div>
            </form>
		</div>
        <div class="card-footer">
            <div class="text-right">
                <input type="submit" value="Simpan" class="btn btn-success" form="form-create">
            </div>
        </div>
	</div>
@endsection