@extends('layout.template')

@push('head')
    <style>
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        input[type=number] {
            -moz-appearance: textfield;
        }
        textarea {
            resize: none;
        }
    </style>
@endpush

@section('content')
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Form Edit Data Cast</h3>
        </div>
		<div class="card-body">
			<form action="{{ url('/cast/' . $edit->id) }}" method="POST" class="form-horizontal" id="form-update">
                @csrf
                @method('PUT')
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"> Nama : </label>
                    <div class="col-sm-4">
                        <input type="text" name="nama" maxlength="50" class="form-control" placeholder="Nama" required autocomplete="off" value="{{ $edit->nama }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"> Umur : </label>
                    <div class="col-sm-4">
                        <input type="number" name="umur" class="form-control" placeholder="Umur" required autocomplete="off" value="{{ $edit->umur }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"> Bio : </label>
                    <div class="col-sm-4">
                        <textarea name="bio" rows="3" class="form-control" placeholder="Bio">{{ $edit->bio }}</textarea>
                    </div>
                </div>
            </form>
		</div>
        <div class="card-footer">
            <div class="text-right">
                <input type="submit" value="Simpan" class="btn btn-success" form="form-update">
            </div>
        </div>
	</div>
@endsection